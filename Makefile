# Install https://github.com/xwmx/hosts
# Also install "spawn-fcgi"

CURVE := secp521r1# https://gitlab.com/gnutls/gnutls/blob/master/lib/algorithms/ecc.c#L34
HASH := sha512# https://gitlab.com/gnutls/gnutls/blob/master/src/certtool-common.c#L1666
SERVER_PATH := test.sock

.PHONY: clean install-client

fullchain.pem: cert.pem cacert.crt
	cat cert.pem cacert.crt > '$@'

clean:
	$(RM) cakey.pem carequest.pem cacert.crt privkey.pem request.pem cert.pem fullchain.pem

cakey.pem:
	certtool -p --no-text --outfile '$@' --ecc --curve '$(CURVE)'

carequest.pem: cakey.pem ca.cfg
	certtool -q --no-text --outfile '$@' --hash '$(HASH)' --template 'ca.cfg' --load-privkey '$<'

cacert.crt: cakey.pem carequest.pem
	certtool -s --no-text --outfile '$@' --hash '$(HASH)' --template 'ca.cfg' --load-privkey '$<' --load-request 'carequest.pem'

privkey.pem:
	certtool -p --no-text --outfile '$@' --ecc --curve '$(CURVE)'

request.pem: privkey.pem server.cfg
	certtool -q --no-text --outfile '$@' --hash '$(HASH)' --template 'server.cfg' --load-privkey '$<'

cert.pem: request.pem cacert.crt cakey.pem
	certtool -c --no-text --outfile '$@' --hash '$(HASH)' --template 'server.cfg' --load-ca-certificate 'cacert.crt' --load-ca-privkey 'cakey.pem' --load-request '$<'




$(SERVER_PATH):
	./test.js '$(SERVER_PATH)'

/etc/nginx/sites-enabled/auth.vintagestory.at.conf: nginx.conf fullchain.pem privkey.pem
	ssl_certificate='$(abspath fullchain.pem)' \
	ssl_certificate_key='$(abspath privkey.pem)' \
	fastcgi_pass='$(abspath $(SERVER_PATH))' \
	envsubst \$$ssl_certificate,\$$ssl_certificate_key,\$$fastcgi_pass < '$<' > '$@'
	nginx -t
	systemctl reload nginx

install-client: cacert.crt
	# todo: use an evironment variable to set redirect ip
	- hosts --auto-sudo block auth.vintagestory.at
	# todo: grab certificate from server instead
	certmgr -add -c -v Trust cacert.crt

	
